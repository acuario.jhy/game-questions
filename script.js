const questions = [
    {
        question: "¿Cuál es la capital de Francia?",
        options: ["París", "Londres", "Madrid", "Roma"],
        answer: "París"
    },
    {
        question: "¿Cuál es el río más largo del mundo?",
        options: ["Nilo", "Amazonas", "Yangtsé", "Misisipi"],
        answer: "Nilo"
    },
    {
        question: "¿En qué año ocurrió la Revolución Francesa?",
        options: ["1789", "1798", "1802", "1810"],
        answer: "1789"
    }
];

let currentQuestion = 0;
let score = 0;

const questionElement = document.getElementById("question");
const optionsElements = document.querySelectorAll(".option");
const resultElement = document.getElementById("result");

function loadQuestion() {
    const current = questions[currentQuestion];
    questionElement.textContent = current.question;
    optionsElements.forEach((element, index) => {
        element.textContent = current.options[index];
        element.addEventListener("click", checkAnswer);
    });
}

function checkAnswer(event) {
    const selectedOption = event.target.textContent;
    const current = questions[currentQuestion];

    if (selectedOption === current.answer) {
        score++;
        resultElement.textContent = "¡Respuesta correcta!";
    } else {
        resultElement.textContent = "Respuesta incorrecta";
    }

    currentQuestion++;
    if (currentQuestion < questions.length) {
        setTimeout(() => {
            resultElement.textContent = "";
            loadQuestion();
        }, 1000);
    } else {
        setTimeout(() => {
            questionElement.textContent = "Juego terminado";
            optionsElements.forEach(element => {
                element.style.display = "none";
            });
            resultElement.textContent = `Puntaje final: ${score} de ${questions.length}`;
        }, 1000);
    }
}

loadQuestion();
